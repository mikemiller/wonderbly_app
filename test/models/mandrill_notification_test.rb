require 'test_helper'

class MandrillNotificationTest < ActiveSupport::TestCase
    setup do
        @notification_open = mandrill_notifications(:open_shipment)
        @notification_click = mandrill_notifications(:click_shipment)
        @notification_send = mandrill_notifications(:send_shipment)
    end

    test 'should include everything' do
        filtered = MandrillNotification.filtered({ email_type: nil, event: nil }).all
        assert_includes filtered, @notification_open
        assert_includes filtered, @notification_click
        assert_includes filtered, @notification_send
    end

    test 'should have a filter for event' do
        filtered = MandrillNotification.filtered({ event: @notification_open.event })
        assert_includes filtered, @notification_open
        assert_not_includes filtered, @notification_click
        assert_not_includes filtered, @notification_send
    end

    test 'should have a filter for email type' do
        notification_userconfirmation = mandrill_notifications(:click_user_confirmation)
        filtered = MandrillNotification.filtered({ email_type: notification_userconfirmation.email_type })
        assert_includes filtered, notification_userconfirmation
        assert_not_includes filtered, @notification_open
        assert_not_includes filtered, @notification_click
        assert_not_includes filtered, @notification_send
    end
end
