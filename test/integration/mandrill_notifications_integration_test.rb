require 'test_helper'

class MandrillNotificationsIntegrationTest < ActionDispatch::IntegrationTest
    test 'get index' do
        get mandrill_notifications_path
        assert_response :success
    end

    test 'index assigns email_type' do
        email_types = %w(shipment userconfirmation, referafriend)
        email_types.each do |email_type|
            get mandrill_notifications_path, email_type: email_type
            assert_response :success
            assert_equal email_type, assigns(:email_type)
        end
    end
end
