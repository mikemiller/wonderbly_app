require 'test_helper'
module Webhooks
    class MandrillIntegrationTest < ActionDispatch::IntegrationTest
        test 'create with valid data creates a Mandrill Notification with correct attributes' do
            assert_difference 'MandrillNotification.count', 1 do
                post webhooks_create_mandrill_webhook_path, '{"Address":"barney@lostmy.name","EmailType":"Shipment","Event":"send","Timestamp":1432820696}'
            end
            mandrill_notification = MandrillNotification.last
            assert_response :success
            assert_equal 'send', mandrill_notification.event
            assert_equal 'barney@lostmy.name', mandrill_notification.email_address
            assert_equal 'shipment', mandrill_notification.email_type
            refute_empty mandrill_notification.raw_payload
        end
    end
end
