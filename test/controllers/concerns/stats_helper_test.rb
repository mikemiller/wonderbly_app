require 'test_helper'

class StatsHelperTest < ActiveSupport::TestCase
    include StatsHelper
    setup do
        @notification_open = mandrill_notifications(:open_shipment)
        @notification_click = mandrill_notifications(:click_shipment)
        @notification_send = mandrill_notifications(:send_shipment)
    end

    test 'calculate_click_rate' do
        click_rate = calculate_click_rate(@notification_open.email_type)
        assert_equal 100, click_rate
    end

    test 'calculate_open_rate' do
        MandrillNotification.create(email_type: 'shipment', event: 'send')
        open_rate = calculate_open_rate(@notification_open.email_type)
        assert_equal 50, open_rate
    end
end
