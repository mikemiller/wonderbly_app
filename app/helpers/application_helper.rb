module ApplicationHelper
    def format_time(t, separator = false)
        return I18n.l(t, format: '%d.%m.%Y - %H:%M:%S') if t && separator
        return I18n.l(t, format: '%d.%m.%Y %H:%M:%S') if t
    end
end
