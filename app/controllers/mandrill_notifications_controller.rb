class MandrillNotificationsController < ApplicationController
    def index
        @mandrill_notifications        = MandrillNotification.filtered(params).order('id desc').page(params[:page])
        @email_type                    = params[:email_type].present? ? params[:email_type] : nil
        @click_rate                    = calculate_click_rate(@email_type)
        @open_rate                     = calculate_open_rate(@email_type)
    end

    def show
        @mandrill_notification = MandrillNotification.find(params[:id])
    end
end
