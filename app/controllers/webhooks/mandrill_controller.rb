module Webhooks
    class MandrillController < ApplicationController
        skip_before_action :verify_authenticity_token, only: [:create]

        def create
            payload = JSON.parse(request.body.read) if request.body
            if %w(send click open).include? payload['Event']
                email_type = payload['EmailType'].downcase if payload['EmailType'].present?
                MandrillNotification.create(event: payload['Event'],
                                            email_address: payload['Address'],
                                            email_type: email_type,
                                            raw_payload: payload)
            end
            render status: 200, nothing: true
        end
    end
end
