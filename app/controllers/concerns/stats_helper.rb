module StatsHelper
    def calculate_click_rate(email_type)
        total_sent = MandrillNotification.where(email_type: email_type, event: 'send').count.to_f
        total_clicked = MandrillNotification.where(email_type: email_type, event: 'click').count.to_f
        click_rate = (total_clicked / total_sent) * 100 unless total_sent.zero?
        click_rate
    end

    def calculate_open_rate(email_type)
        total_sent = MandrillNotification.where(email_type: email_type, event: 'send').count.to_f
        total_opened = MandrillNotification.where(email_type: email_type, event: 'open').count.to_f
        open_rate = (total_opened / total_sent.to_f) * 100 unless total_sent.zero?
        open_rate
    end
end
