class MandrillNotification < ActiveRecord::Base
    def self.filtered(params)
        return self if params.nil?
        result = self
        unless params[:event].blank?
            result = result.where(event: params[:event])
        end
        unless params[:email_type].blank?
            result = result.where(email_type: params[:email_type])
        end
        result
    end
end
