# README
This is a Rails application that recieves Mandrill notifications and displays them in a dashboard. This app is built with a PostgreSQL database.

I aimed to implement a web application in Ruby that:
* can receive webhooks from Mandrill
* processes the webhook data and stores it into a suitable data store
* displays the following statistics
    - total number of emails sent
    - total number of emails opened
    - total number of clicks
    - open rate per email type
    - click rate per email type

### How To Set Up Locally
In your terminal run:
```sh
    $ git clone https://gitlab.com/mikemiller/wonderbly_app.git
    $ cd wonderbly_app
```
run the following command to install all required gems:
```sh
    $ bundle install
```
then:
```sh
    $ rake db:setup
```
then (optional):
```sh
    $ rake db:seed
```
then:
```sh
    $ rails s
```
to run the tests...in a new terminal window:
```sh
    $ guard
```
Guard will run the corresponding tests when you save a file.

alternatively:
```sh
    $ rake test
```

The endpoint for notifications is: '/webhooks/mandrill/create'
### N.B. To see the click rate and open rate values, please first filter by email type.

While Rails is not needed for a project this size and a similar result could have been achieved with Ruby, Redis and Sinatra - which would be faster and more streamline - I wanted to show my familiarity with the Rails framework, ActiveRecord etc. I see this fitting into a larger application with related models.

To extend this, I would like to add security checks in the MandrillController to ensure incoming webhooks are from Mandrill and then return appropriate status codes:
```sh
    render status: 400 unless sig_header
    render status: 401 unless authorized
```
As it's possible that Mandrill could send us the same notification twice, it would be a good idea to add Mandrill's own notification_ids to a Redis Set (only permits unique values) inside the MandrillController and check if the mandrill_notification_id already exists before creating a new object in ActiveRecord.

If I had more time, I think it would be worthwhile to make a graph from our MandrillNotification data and display how the open and click rate changes over time.

Depending on the number of notifications (the size of our mailing list etc), we might want a worker to clear old notifications every few months or so.

