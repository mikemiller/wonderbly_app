Rails.application.routes.draw do
    root :to => 'mandrill_notifications#index'
    namespace :webhooks do
        post 'mandrill/create', to: 'mandrill#create', as: 'create_mandrill_webhook'
    end
    resources :mandrill_notifications, only: [:index, :show]
end
