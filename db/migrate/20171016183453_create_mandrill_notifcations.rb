class CreateMandrillNotifcations < ActiveRecord::Migration
  def change
    create_table :mandrill_notifications do |t|
        t.string :event
        t.string :email_address
        t.string :email_type
        t.json :raw_payload
        t.timestamps null: false
    end
  end
end
